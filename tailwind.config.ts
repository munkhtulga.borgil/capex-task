import type { Config } from "tailwindcss";

const config: Config = {
  prefix: "tw-",
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    screens: {
      sm: "480px",
      md: "768px",
      lg: "1024px",
      xl: "1366px",
      xxl: "1920px",
    },
    extend: {
      colors: {
        primary: "#212833",
        secondary: "#595959",
        brand: "#FF008C",
        darkDeep: "#001029",
        grayThin: "#929AA5",
        grayLight: "#E0E0E0",
        graySoft: "#5F6368",
        grayMedium: "#D9D9D9",
        grayBackdrop: "#FAFAFA",
        greenBackdrop: "rgba(46, 189, 133, 0.15)",
        greenLight: "#2EBD85",
        cyanBackdrop: "rgba(0, 196, 205, 0.15)",
        blueBackdrop: "rgba(0, 196, 205, 0.2)",
        blueSoft: "#1E9AD4",
        blueMedium: "#3F6FDA",
        redLight: "#F5465C",
        redSoft: "rgba(255, 0, 140, 0.1)"
      },
    },
  },
  plugins: [],
};
export default config;
