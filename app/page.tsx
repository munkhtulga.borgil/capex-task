"use client";

import BannerSection from "./components/main-page/BannerSection";
import OpenAccountSection from "./components/main-page/OpenAccountSection";
import TrendingCoinsSection from "./components/main-page/TrendingCoinsSection";
import CoinListItemInterface from "@/app/models/CoinListItemInterface";
import AutoInvestmentSection from "./components/main-page/AutoInvestmentSection";
import FeaturesSection from "./components/main-page/FeaturesSection";
import TradeNowSection from "./components/main-page/TradeNowSection";

export default function Home() {
  const coinList: CoinListItemInterface[] = [
    {
      id: 1,
      type: 1,
      name: "BTC - Bitcoin",
      price: 17206,
      totalMarketAssessment: 331472240000,
      demandingAmount: 1925756,
      change: 1.52,
      isAscending: true,
    },
    {
      id: 2,
      type: 2,
      name: "ETH - Ethereum",
      price: 17206,
      totalMarketAssessment: 424232240000,
      demandingAmount: 1925756,
      change: 1.52,
      isAscending: false,
    },
    {
      id: 3,
      type: 3,
      name: "USDT - Tether",
      price: 17206,
      totalMarketAssessment: 1272240000,
      demandingAmount: 1925756,
      change: 1.52,
      isAscending: true,
    },
    {
      id: 4,
      type: 1,
      name: "BTC - Bitcoin",
      price: 17206,
      totalMarketAssessment: 331472240000,
      demandingAmount: 1925756,
      change: 1.52,
      isAscending: true,
    },
    {
      id: 5,
      type: 2,
      name: "ETH - Ethereum",
      price: 17206,
      totalMarketAssessment: 424232240000,
      demandingAmount: 1925756,
      change: 1.52,
      isAscending: false,
    },
    {
      id: 6,
      type: 3,
      name: "USDT - Tether",
      price: 17206,
      totalMarketAssessment: 1272240000,
      demandingAmount: 1925756,
      change: 1.52,
      isAscending: true,
    },
  ];

  return (
    <main className="tw-grid tw-grid-cols-1 tw-auto-rows-min">
      <BannerSection />
      <div className="tw-py-[87px] tw-px-[139px] xxl:tw-px-[244px]">
        <OpenAccountSection />
        <TrendingCoinsSection list={coinList} />
        <AutoInvestmentSection />
        <FeaturesSection />
        <TradeNowSection />
      </div>
    </main>
  );
}
