interface FooterMenuItem {
  text: string;
  icon?: string;
  route?: string;
}

interface FooterMenuBlock {
  [key: string]: FooterMenuItem[];
}

export type FooterMenus = {
  blockOne: FooterMenuItem[];
  blockTwo: FooterMenuItem[];
  blockThree: FooterMenuItem[];
  blockFour: FooterMenuItem[];
} & FooterMenuBlock;