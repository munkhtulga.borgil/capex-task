interface listItem {
    id: number,
    type: number,
    name: string,
    price: number,
    totalMarketAssessment: number,
    demandingAmount: number,
    change: number,
    isAscending: boolean
  }

export default listItem