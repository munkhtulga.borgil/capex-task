export const thousandSeparator = (value: unknown): string => {
  let result = "0";
  if (value && typeof value === "number") {
    result = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  return result;
};

export const calculateRelativePercentage = (
  part: unknown,
  whole: unknown
): string => {
  let result = "0";
  if (part && whole && typeof part === "number" && typeof whole === "number") {
    if (whole === 0) {
      return result;
    }

    result = ((part / whole) * 100).toString();
  }
  return result;
};
