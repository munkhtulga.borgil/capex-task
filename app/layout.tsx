import "./globals.css";
import type { Metadata } from "next";
import { IBM_Plex_Sans } from "next/font/google";

// Component imports
import NavBar from "./components/NavBar";
import MainFooter from "./components/MainFooter";

const IBMPlexSans = IBM_Plex_Sans({
  weight: "400",
  subsets: ["latin", "cyrillic-ext"],
});

export const metadata: Metadata = {
  title: "Capex",
  description: "Capex Task App",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={IBMPlexSans.className}>
        <div className="tw-fixed tw-top-0 tw-right-0 tw-left-0 tw-z-50">
          <NavBar />
        </div>
        <div className="tw-pt-[82px]">{children}</div>
        <MainFooter />
      </body>
    </html>
  );
}
