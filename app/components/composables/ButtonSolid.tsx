interface ButtonSolidProps {
  text: string;
  width?: string | number;
  height?: string | number;
  radius?: string | number;
  textSize?: string | number;
  textHeight?: string | number;
  textWeight?: string | number;
  disabled?: boolean
}

export default function ButtonSolid({
  text,
  width,
  height,
  radius,
  textSize,
  textHeight,
  textWeight,
  disabled
}: ButtonSolidProps) {
  return (
    <>
      <button
        style={{
            backgroundColor: disabled ? 'rgba(89, 89, 89, 0.4)' : 'rgba(0, 196, 205, 0.2)',
          width: width ? `${width}px` : "100%",
          height: height ? `${height}px` : "47px",
          borderRadius: radius ? `${radius}px` : "3px",
          cursor: disabled ? 'not-allowed' : 'pointer'
        }}
        disabled={disabled}
        className="tw-rounded-[5px] tw-grid tw-place-items-center"
      >
        <span
          style={{
            color: disabled ? '#FFFFFF' : '#1E9AD4',
            fontSize: textSize ? `${textSize}px` : "17px",
            lineHeight: textHeight ? `${textHeight}px` : "22.1px",
            fontWeight: textWeight ? textWeight : 400,
          }}
        >
          {text}
        </span>
      </button>
    </>
  );
}