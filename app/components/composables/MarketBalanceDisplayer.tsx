import {
  calculateRelativePercentage,
  thousandSeparator,
} from "@/app/utils/prototypes";

interface MarketBalanceDisplayerProps {
  marketMax: number;
  marketCurrent: number;
}

export default function MarketBalanceDisplayer({
  marketMax,
  marketCurrent,
}: MarketBalanceDisplayerProps) {
  return (
    <div className="tw-relative tw-h-[31px] tw-rounded-[3px] tw-overflow-hidden tw-flex tw-justify-center tw-items-center">
      <div className="tw-absolute tw-z-0 tw-top-0 tw-bottom-0 tw-left-0 tw-right-0 tw-bg-grayBackdrop"></div>
      <div
        style={{
          width: `${calculateRelativePercentage(marketCurrent, marketMax)}%`,
        }}
        className="tw-absolute tw-z-10 tw-top-0 tw-bottom-0 tw-left-0 tw-bg-greenBackdrop"
      ></div>
      <span className="tw-z-20 tw-text-[14px] tw-leading-[18.2px]">{`${thousandSeparator(
        marketCurrent
      )} MNT / ${thousandSeparator(marketMax)} MNT`}</span>
    </div>
  );
}
