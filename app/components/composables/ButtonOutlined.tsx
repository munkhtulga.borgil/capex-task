interface ButtonOutlinedProps {
  text: string;
  width?: string | number;
  height?: string | number;
  radius?: string | number
}

export default function ButtonOutlined({
  text,
  width,
  height,
  radius
}: ButtonOutlinedProps) {
  return (
    <>
      <button
        style={{
          width: width ? `${width}px` : "117px",
          height: height ? `${height}px` : "37px",
          borderRadius: radius ? `${radius}px` : "5px",
        }}
        className="button-outlined tw-relative tw-rounded-[5px] tw-grid tw-place-items-center"
      >
        <div className="inner-wrapper">
            <span className="tw-text-primary tw-text-base tw-leading-5">{text}</span>
        </div>
      </button>
    </>
  );
}
