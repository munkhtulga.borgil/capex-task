interface ButtonPrimaryProps {
  text: string;
  width?: string | number;
  height?: string | number;
  radius?: string | number;
  textSize?: string | number;
  textHeight?: string | number;
  textWeight?: string | number;
}

export default function ButtonPrimary({
  text,
  width,
  height,
  radius,
  textSize,
  textHeight,
  textWeight,
}: ButtonPrimaryProps) {
  return (
    <>
      <button
        style={{
          width: width ? `${width}px` : "117px",
          height: height ? `${height}px` : "37px",
          borderRadius: radius ? `${radius}px` : "5px",
        }}
        className="button-primary tw-rounded-[5px] tw-grid tw-place-items-center"
      >
        <span
          style={{
            fontSize: textSize ? `${textSize}px` : "17px",
            lineHeight: textHeight ? `${textHeight}px` : "22.1px",
            fontWeight: textWeight ? textWeight : 400,
          }}
          className="tw-text-white"
        >
          {text}
        </span>
      </button>
    </>
  );
}
