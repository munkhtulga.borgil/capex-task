import Image from "next/image";

interface InputPurchaseProps {
  currencyName: string;
  icon: string;
  placeholder?: string;
  disabled?: boolean;
}

export default function InputPurchase({
  currencyName,
  icon,
  placeholder,
}: InputPurchaseProps) {
  return (
    <div className="tw-h-[55px] tw-px-[23px] tw-py-[15px] tw-flex tw-justify-start tw-items-center tw-gap-2 tw-border-[1px] tw-border-secondary/60 tw-rounded-[3px]">
      <input
        type="text"
        placeholder={placeholder ? placeholder : ""}
        className="focus:tw-outline-none tw-bg-transparent tw-w-full tw-h-full"
      />
      <Image src={icon} width={27} height={27} alt="current-icon" />
      <span className="tw-font-medium tw-text-[16px] tw-leading-[20.8px] tw-text-secondary">
        {currencyName}
      </span>
      <button className="tw-bg-blueBackdrop tw-grid tw-place-items-center tw-w-[51px] tw-h-[25px] tw-rounded-[3px]">
        <span className="tw-text-blueSoft tw-font-medium tw-text-[16px] tw-leading-[20.8px]">
          Max
        </span>
      </button>
    </div>
  );
}
