import Image from "next/image";
import FooterMenusData from "@/app/resources/FooterMenus.json";
import { FooterMenus } from "../models/FooterMenuInterface";
import Logo from "@/public/static/capex-logo-no-text.svg";
import QrCode from "@/public/static/images/qr-code.png";
import WarningIcon from "@/public/static/icons/warning-icon.svg";
import Link from "next/link";

export default function MainFooter() {
  const footerMenus: FooterMenus = FooterMenusData;

  const socialLinks: Array<Record<string, string>> = [
    {
      icon: "/static/icons/social/reddit-icon.svg",
      url: "/",
    },
    {
      icon: "/static/icons/social/discord-icon.svg",
      url: "/",
    },
    {
      icon: "/static/icons/social/twitter-icon.svg",
      url: "/",
    },
    {
      icon: "/static/icons/social/facebook-icon.svg",
      url: "/",
    },
    {
      icon: "/static/icons/social/instagram-icon.svg",
      url: "/",
    },
    {
      icon: "/static/icons/social/telegram-icon.svg",
      url: "/",
    },
    {
      icon: "/static/icons/social/linkedin-icon.svg",
      url: "/",
    },
  ];

  return (
    <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-6">
      <section className="tw-bg-darkDeep tw-py-[45px] tw-px-[239px]">
        <div className="tw-flex tw-flex-col tw-gap-1">
          <div className="tw-flex tw-justify-start tw-items-center tw-gap-[15px]">
            <Image
              src={WarningIcon}
              width={40}
              height={40}
              quality={100}
              alt="warning-icon"
            />
            <span className="tw-font-[600] tw-text-[36px] tw-leading-[46.8px] tw-text-white">
              Анхааруулга
            </span>
          </div>
          <div className="tw-text-[24px] tw-leading-[38.4px] tw-text-white tw-text-justify">
            Виртуал хөрөнгийн биржийн арилжаанд оролцох нь санхүүгийн томоохон
            эрсдэлийг дагуулж байдаг тул та зохих хэмжээнд мэдлэг туршлагатай
            байх эсвэл мэргэжлийн байгууллага, хувь хүнээс арилжаатай холбоотой
            заавар зөвлөгөөг авсаны дараагаар хөрөнгө оруулалтын шийдвэрээ
            гаргахыг зөвлөж байна. Та аливаа шийдвэрээ зөвхөн өөрөө гаргах
            шаардлагатай бөгөөд арилжаанаас үүссэн ямар нэгэн үр дүнд бирж
            хариуцлага хүлээхгүй болохыг анхаарна уу.
          </div>
        </div>
      </section>
      <section className="tw-border-t-[0.5px] tw-border-[#000000]/10 tw-pt-[38px] tw-pb-[29.5px] tw-px-[64px]">
        <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-[35px]">
          <div className="tw-flex tw-justify-start tw-items-center tw-gap-10 xxl:tw-gap-[133px]">
            <div className="tw-min-w-[69px]">
              <Image
                src={Logo}
                height={69}
                width={69}
                quality={100}
                alt="capex-logo"
              />
            </div>
            <div className="tw-grow tw-grid tw-grid-cols-4 tw-auto-rows-min tw-gap-6 xxl:tw-gap-[122px]">
              {Object.keys(footerMenus).map((key) => {
                return (
                  <section key={key}>
                    <ul className="tw-m-0 tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-[13px]">
                      {footerMenus[key].map((item, idx) => {
                        return (
                          <li
                            key={item.text}
                            className="tw-flex tw-justify-start tw-items-center tw-gap-[15px]"
                          >
                            {item.icon && (
                              <Image
                                src={item.icon}
                                width={24}
                                height={24}
                                quality={100}
                                alt={`icon-${idx + 1}`}
                              />
                            )}
                            <span className="tw-text-secondary tw-font-[300] tw-text-[16px] tw-leading-[20.8px]">
                              {item.route ? (
                                <Link href={item.route}>{item.text}</Link>
                              ) : (
                                item.text
                              )}
                            </span>
                          </li>
                        );
                      })}
                    </ul>
                  </section>
                );
              })}
            </div>
            <div className="tw-min-w-[106px]">
              <Image
                src={QrCode}
                height={106}
                width={106}
                quality={100}
                alt="qr-code"
              />
            </div>
          </div>
          <div className="tw-h-[0.5px] tw-w-full tw-bg-grayThin"></div>
          <div className="tw-flex tw-justify-between tw-items-center">
            <section>
              <span className="tw-text-secondary tw-font-[300] tw-text-[16px] tw-leading-[20.8px]">
                Зохиогчийн эрх © 2022 &quot;Капекс Капитал&quot; ХХК Бүх эрх
                хуулиар хамгаалагдсан.
              </span>
            </section>
            <section className="tw-flex tw-justify-start tw-items-center tw-gap-[7px]">
              {socialLinks.map((link, idx) => {
                return (
                  <Link key={link.icon} href={link.url}>
                    <Image
                      src={link.icon}
                      height={32}
                      width={32}
                      quality={100}
                      alt={`link-${idx + 1}`}
                    />
                  </Link>
                );
              })}
            </section>
          </div>
        </div>
      </section>
    </div>
  );
}
