import Image from "next/image";
import NotificationIcon from "@/public/static/icons/notification-bulb-icon.svg";

export default function PageReminder() {
  return (
    <div className="tw-bg-redSoft tw-h-[44px] tw-flex tw-justify-center tw-items-center">
      <section className="tw-flex tw-justify-start tw-items-center tw-gap-[34px]">
        <Image
          src={NotificationIcon}
          height={25}
          width={24}
          quality={100}
          alt="notification-icon"
        />
        <span className="tw-text-[15px] tw-leading-[19.5px] tw-text-graySoft">
          Урамшууллын хугацаа 2022-04-15 16:00 цагт дууссан
        </span>
      </section>
    </div>
  );
}
