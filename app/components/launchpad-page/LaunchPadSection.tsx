import Card from "@/app/components/launchpad-page/LaunchPadCard";

export default function LaunchPadSection() {
  return (
    <div className="tw-px-[226px] tw-pb-[78px] tw-grid tw-grid-cols-2 xxl:tw-grid-cols-3 tw-auto-rows-min tw-gap-x-[110px] tw-gap-y-[37px]">
      {Array.from({ length: 6 }, (_, index) => (
        <Card key={index} id={index + 1} />
      ))}
    </div>
  );
}
