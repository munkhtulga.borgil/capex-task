import CreditCoinImage from "@/public/static/images/credit-coin.png";
import TokenImage from "@/public/static/images/industrial-token.png";
import Image from "next/image";
import Link from "next/link";

interface LaunchPadCardProps {
  id: string | number;
}

export default function LaunchPadCard({ id }: LaunchPadCardProps) {
  return (
    <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-[30px] tw-overflow-hidden">
      <Image
        src={CreditCoinImage}
        width={0}
        height={0}
        quality={100}
        style={{ width: "100%", height: "auto" }}
        alt="launch-pad"
      />
      <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min">
        <div className="tw-flex tw-justify-between tw-items-center">
          <span className="tw-text-[22px] tw-font-[600] tw-leading-[28.6px]">
            CreditCoin Mongolia
          </span>
          <span className="tw-py-1 tw-px-2 tw-bg-greenBackdrop tw-rounded-[3px] tw-text-greenLight tw-font-medium tw-text-[16px] tw-leading-[19.2px]">
            2022.09.12 | 17:28:43
          </span>
        </div>
        <div className="tw-mt-3">
          <span className="tw-text-[16px] tw-leading-[20.8px]">
            CreditCoin нь хүн бүр оролцох боломжтой хөрөнгө оруулалтын
            экосистемийг хөгжүүлж,
          </span>
        </div>
        <div className="tw-flex tw-justify-end tw-mt-[3px]">
          <Link href={`/launchpad/${id}`}>
            <button className="tw-bg-cyanBackdrop tw-rounded-[28px] tw-py-[7px] tw-px-[17px]">
              <span className="tw-text-blueMedium tw-font-medium tw-text-[14px] tw-leading-[18.2px]">
                Цааш үзэх
              </span>
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
}
