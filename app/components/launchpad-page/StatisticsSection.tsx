export default function StatisticsSection() {
  const statisticsList: Array<Record<string, string | number>> = [
    {
      text: "$0",
      description: "Одоогийн сан",
    },
    {
      text: "$108,519,833",
      description: "Нийт хөрөнгө",
    },
    {
      text: "66",
      description: "Идэвхтэй төсөл",
    },
    {
      text: "3,255",
      description: "Оролцогчдын тоо",
    },
  ];

  return (
    <div className="tw-bg-grayBackdrop tw-h-[199px] tw-px-[226px] tw-flex tw-items-center">
      <div className="tw-grow tw-grid tw-grid-cols-4 tw-auto-rows-min">
        {statisticsList.map((item) => {
          return (
            <section key={item.text} className="tw-flex tw-flex-col tw-gap-3">
              <span className="tw-font-medium tw-text-[28px] tw-leading-[36.4px]">
                {item.text}
              </span>
              <span className="tw-text-[20px] tw-leading-[26px]">
                {item.description}
              </span>
            </section>
          );
        })}
      </div>
    </div>
  );
}
