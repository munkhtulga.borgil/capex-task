import Image from "next/image";
import Banner from "@/public/static/images/launchpad-banner.png";

export default function BannerSection() {
  return (
    <div className="tw-relative">
      <Image
        src={Banner}
        width={0}
        height={0}
        sizes="100vw"
        style={{ width: "100%", height: "auto" }}
        quality={100}
        priority
        alt="banner"
      />
      <div className="tw-absolute tw-left-[900px] tw-top-[120px] xxl:tw-top-[188px] xxl:tw-left-[1124px] tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-[6px]">
        <span className="tw-text-white tw-text-[48px] tw-leading-[62.4px] tw-font-bold">
          LAUNCHPAD
        </span>
        <span className="tw-text-white tw-text-[20px] tw-leading-[26px]">
          Шинэ токен мэдээлэл болон худалдаж авах
        </span>
      </div>
    </div>
  );
}
