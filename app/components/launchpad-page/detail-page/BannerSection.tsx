import Banner from "@/public/static/images/launchpad-banner.png"
import Image from "next/image"

export default function BannerSection() {
  return (
    <div className="tw-max-h-[294px] tw-overflow-hidden tw-relative">
        <Image src={Banner} width={0} height={0} quality={100} priority style={{width: '100%', height: 'auto'}} alt="banner" />
        <div className="tw-absolute tw-top-1/2 tw-translate-y-[-50%] tw-left-[800px] xxl:tw-left-[914px] tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-[6px]">
        <span className="tw-text-white tw-text-[48px] tw-leading-[62.4px] tw-font-bold">
          LAUNCHPAD
        </span>
        <span className="tw-text-white tw-text-[20px] tw-leading-[26px]">
          Шинэ токен мэдээлэл болон худалдаж авах
        </span>
      </div>
    </div>
  )
}
