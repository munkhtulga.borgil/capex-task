import { thousandSeparator } from "@/app/utils/prototypes";
import MarketBalanceDisplayer from "../../composables/MarketBalanceDisplayer";
import InputPurchase from "../../composables/InputPurchase";
import ButtonSolid from "../../composables/ButtonSolid";

interface DetailItemType {
  title: string;
  text: string;
  description?: string;
}

export default function PurchaseSection() {
  const detailsList: DetailItemType[] = [
    {
      title: "Токены нэр",
      text: "Simple Coin",
    },
    {
      title: "Токен гаргагч",
      text: "Crypto Company LLC",
    },
    {
      title: "Нэг хүний дээд хэмжээ",
      text: "30,000,000 SEC",
      description: "15,000,000 MNT",
    },
    {
      title: "Токены тикер",
      text: "SEC",
    },
    {
      title: "Нийт токены хэмжээ",
      text: "100,000,000 SEC",
    },
    {
      title: "Эхлэх огноо",
      text: "2022.09.12",
      description: "14:00:00",
    },
    {
      title: "Нэгж үнэ",
      text: "1 SEC = 0.3MNT",
    },
    {
      title: "Дотоод зах зээлд",
      text: "100,000,000 SEC",
    },
    {
      title: "Дуусах огноо",
      text: "2022.10.03",
      description: "23:59:59",
    },
  ];

  return (
    <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min">
      <section className="tw-flex tw-flex-col tw-gap-[9px]">
        <div className="tw-flex tw-justify-between tw-items-end">
          <span className="tw-text-[28px] tw-leading-[36.4px] tw-font-medium">
            Simple Coin
          </span>
          <span className="tw-text-secondary tw-text-[16px] tw-leading-[20.8px] tw-font-medium">
            Дуусах огноо
          </span>
        </div>
        <div className="tw-flex tw-justify-between tw-items-end">
          <span className="tw-text-secondary tw-text-[18px] tw-leading-[23.4px]">
            Official financial payment solutions
          </span>
          <span className="tw-text-[20px] tw-leading-[26px] tw-font-medium">
            2022-12-01
          </span>
        </div>
      </section>
      <section className="tw-grid tw-grid-cols-3 tw-gap-y-[18px] tw-mt-[29px]">
        {detailsList.map((item) => {
          return (
            <div key={item.title} className="tw-flex tw-flex-col tw-gap-[2px]">
              <span className="tw-text-secondary tw-text-[16px] tw-leading-[20.8px]">
                {item.title}
              </span>
              <span className="tw-text-secondary tw-text-[18px] tw-leading-[23.4px] tw-font-medium">
                {item.text}
              </span>
              {item.description && (
                <span className="tw-text-secondary tw-text-[14px] tw-leading-[18.2px]">
                  {item.description}
                </span>
              )}
            </div>
          );
        })}
      </section>
      <section className="tw-mt-[28px]">
        <MarketBalanceDisplayer
          marketCurrent={2643941655}
          marketMax={4500000000}
        />
      </section>
      <section className="tw-mt-[45px] tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-5">
        <InputPurchase
          currencyName={"SEC"}
          icon={"/static/icons/sec-icon.svg"}
          placeholder={"Доод хэмжээ 200,000 SEC"}
        />
        <InputPurchase
          currencyName={"MNT"}
          icon={"/static/icons/mnt-icon.svg"}
          placeholder={"1 SEC = 0.15 MNT"}
        />
      </section>
      <section className="tw-mt-10 tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-2">
        <div className="tw-flex tw-justify-between tw-items-end">
          <span className="tw-text-[14px] tw-leading-[18.2px] tw-text-secondary">
            Боломжит үлдэгдэл:
          </span>
          <span className="tw-text-[14px] tw-leading-[18.2px]">{`${thousandSeparator(
            6512135798.32
          )} MNT`}</span>
        </div>
        <ButtonSolid
          text={"Авах"}
          textSize={18}
          textHeight={23.4}
          textWeight={500}
          disabled={true}
        />
      </section>
      <section className="tw-mt-5">
        <span className="tw-text-[12px] tw-leading-[15.6px]">
          Whitepaper болон койны дэлгэрэнгүй мэдээлэлтэй танилцсаны үр дүнд
          шийдвэрээ гаргана уу! Крипто валютын үнэ цэнэ нь зах зээлд оролцогчдын
          эрэлт болон нийлүүлэлтээс хамаардаг тул та үнийн хэлбэлзлээс алдагдал
          болон ашиг хүртэх боломжтойг анхаарна уу!
        </span>
      </section>
    </div>
  );
}
