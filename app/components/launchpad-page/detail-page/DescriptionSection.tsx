import Image from "next/image";
import DemoImage from "@/public/static/images/launchpad-detail-demo.png";
import FacebookIcon from "@/public/static/icons/social/facebook-icon.svg";
import InstagramIcon from "@/public/static/icons/social/instagram-icon.svg";
import LinkedInIcon from "@/public/static/icons/social/linkedin-icon.svg";
import Link from "next/link";

export default function DescriptionSection() {
  return (
    <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min">
      <Image
        src={DemoImage}
        width={0}
        height={0}
        quality={100}
        priority
        style={{ width: "100%", height: "auto" }}
        alt="launchpad-image"
      />
      <section className="tw-mt-[18px]">
        <span className="tw-font-medium tw-text-[28px] tw-leading-[36.4px]">
          Simple Coin Тухай дэлгэрэнгүй мэдээлэл
        </span>
      </section>
      <section className="tw-mt-[16px] tw-flex tw-flex-col tw-gap-[7px]">
        <span className="tw-text-secondary tw-text-[20px] tw-leading-[26px] tw-font-medium">
          Төслийн танилцуулга
        </span>
        <div className="primary-linear-gradient-background tw-h-[3px] tw-w-[203px]"></div>
      </section>
      <section className="tw-mt-[17px] tw-text-justify">
        <span className="tw-text-secondary tw-text-[18px] tw-leading-[23.4px]">
          Hooked Protocol is building the on-ramp layer for massive Web3
          adoption, providing tailored Learn & Earn products and onboarding
          infrastructures for users & businesses to enter the new world of web3.
          Its first pilot product, Wild Cash, with Quiz-to-Earn experience and
          other gamified learning features, achieved an impressive growth of
          over 2 million monthly active users. Hooked Protocol adopts an
          innovative single token (HOOK) oriented structure, supplemented with
          in-ecosystem only utility token HGT (Hooked Gold Token). HOOK is the
          governance token of the ecosystem.
        </span>
      </section>
      <section className="tw-mt-[15px]">
        <span className="tw-text-secondary tw-text-[20px] tw-leading-[26px] tw-font-medium">
          Сошиал холбосууд
        </span>
      </section>
      <section className="tw-mt-[9px]">
        <div className="tw-flex tw-justify-start tw-items-center tw-gap-[7px]">
          <Link href={"/"}>
            <Image
              src={FacebookIcon}
              width={32}
              height={32}
              quality={100}
              alt="fb-icon"
            />
          </Link>
          <Link href={"/"}>
            <Image
              src={InstagramIcon}
              width={32}
              height={32}
              quality={100}
              alt="ig-icon"
            />
          </Link>
          <Link href={"/"}>
            <Image
              src={LinkedInIcon}
              width={32}
              height={32}
              quality={100}
              alt="li-icon"
            />
          </Link>
        </div>
      </section>
    </div>
  );
}
