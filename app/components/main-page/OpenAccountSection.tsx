import AccountIllustration from '@/public/static/illustrations/open-account-illustration.svg'
import ButtonPrimary from '../composables/ButtonPrimary'
import Image from 'next/image'

export default function OpenAccountSection() {
  return (
    <div className="tw-flex tw-justify-between tw-items-start tw-gap-[46px]">
        <section className="tw-grid tw-grid-cols-1 tw-auto-rows-min">
            <span className="tw-text-[36px] tw-font-[600] tw-leading-[46.8px]">Байгууллагын данс нээх</span>
            <span className="tw-text-[24px] tw-leading-[38.4px] tw-mt-[25px]">“Modulus” нь 2019 онд секундэд 10 сая гүйлгээг 40 нано секундийн хоцрогдолтой хурдтай хийх биржийн шийдлээрээ олон улсын “Benzinga Global Fintech Awards”-ын шагналыг хүртсэн.</span>
            <div className="tw-flex tw-justify-start tw-mt-[45px]">
                <ButtonPrimary text={'Бүртгүүлэх'} width={266} height={59} textHeight={26} textSize={20} />
            </div>
        </section>
        <Image src={AccountIllustration} width={350} height={350} quality={100} alt="illustration-account" />
    </div>
  )
}
