import Image from "next/image";
import AutoInvestmentIllustration from "@/public/static/illustrations/auto-investment-package-illustration.svg"
import ButtonPrimary from "../composables/ButtonPrimary";

export default function AutoInvestmentSection() {
    interface ListItem {
        img: string,
        text: string,
        description: string,
    }

    const list: ListItem[] = [
        {
            img: 'static/illustrations/available-coins-illustration.svg',
            text: 'Боломжит койн',
            description: 'Автомат багцад хамаарах койн жагсаалт.'
        },
        {
            img: 'static/illustrations/create-package-illustration.svg',
            text: 'Автомат хөрөнгө оруулалтын багц үүсгэх',
            description: 'Койноо сонгоод хөрөнгө оруулалт болон хугацааг тохируулна.'
        },
        {
            img: 'static/illustrations/earnings-illustration.svg',
            text: 'Орлого олох',
            description: 'Автомат хөрөнгө оруулалтын ашиг болон бусад мэдээлэл өдөр бүр танд очих болно.'
        }
    ]

  return (
    <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-[54px] tw-mt-[104px]">
        <section className="tw-flex tw-flex-col tw-gap-3">
            <span className="tw-font-[600] tw-text-[36px] tw-leading-[46.8px]">Автомат хөрөнгө оруулалтын багц</span>
            <span className="tw-text-[24px] tw-leading-[31.2px]">Манай автомат систем нь арилжааны төлөвийг тодорхойлон автомат багцыг танд санал болгож байна.</span>
        </section>
        <section className="tw-flex tw-justify-between tw-items-center tw-gap-10 xxl:tw-gap-[139px]">
                <ul className="tw-m-0 tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-[38px]">
                    {list.map((item, idx) => {
                        return (
                            <li key={item.text} className="tw-flex tw-justify-start tw-items-center tw-gap-[50px]">
                        <Image src={item.img} width={100} height={100} quality={100} alt={`illustration-${idx + 1}`} />
                        <div className="tw-flex tw-flex-col tw-gap-[6px]">
                            <span className="tw-text-[28px] tw-leading-[36.4px] tw-font-medium">{item.text}</span>
                            <span className="tw-text-[24px] tw-leading-[31.2px]">{item.description}</span>
                        </div>
                    </li>
                        )
                    })}
                </ul>
                <Image src={AutoInvestmentIllustration} height={353} width={500} quality={100} alt="main-illustration" />
        </section>
        <section className="tw-flex tw-justify-start">
                <ButtonPrimary text={"Дэлгэрэнгүй"} width={266} height={59} textSize={20} textHeight={26} />
        </section>
    </div>
  )
}
