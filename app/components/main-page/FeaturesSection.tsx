import Image from "next/image";

export default function FeaturesSection() {
  interface FeatureInterface {
    img: string;
    text: string;
    description: string;
  }

  const list: FeatureInterface[] = [
    {
      img: "static/icons/help-center-icon.svg",
      text: "Тусламжийн төв",
      description:
        "Та манай харилцагчийн үйлчилгээг авч өөрийн асуудлыг шийдвэрлээрэй",
    },
    {
      img: "static/icons/capex-game-icon.svg",
      text: "Capex Game",
      description:
        "Capex Game хөгжөөнт тоглоомд оролцон урамшуулал авах боломж",
    },
    {
      img: "static/icons/capex-community-icon.svg",
      text: "Capex Коммунити",
      description:
        "Дэлхийн зах зээлд нэвтрэх гүүрийг хамдтаа бүтээе.  Capex коммунитид нэгдээрэй",
    },
  ];

  return (
    <div className="tw-mt-[198px] tw-grid tw-grid-cols-3 tw-auto-rows-min tw-gap-[39px]">
      {list.map((item, idx) => {
        return (
          <section
            key={item.text}
            className="tw-relative tw-overflow-hidden tw-rounded-[10px] tw-py-[15px] tw-px-[20px]"
          >
            <div className="feature-card"></div>
            <div className="tw-grid tw-grid-cols-1 tw-place-items-center tw-gap-[14px] ">
              <Image
                src={item.img}
                height={70}
                width={70}
                quality={100}
                alt={`feature-illustration-${idx + 1}`}
              />
              <span className="tw-font-medium tw-text-[18px] tw-leading-[23.4px]">
                {item.text}
              </span>
              <span className="tw-text-[18px] tw-leading-[28.8px] tw-text-center">
                {item.description}
              </span>
            </div>
          </section>
        );
      })}
    </div>
  );
}
