import { thousandSeparator } from "@/app/utils/prototypes";
import CoinListItemInterface from "@/app/models/CoinListItemInterface";
import Image from "next/image";
import CapexTextVector from "@/public/static/images/capex-text-table.svg";
import SortIcon from "@/public/static/icons/sort-vector.svg";
import ButtonPrimary from "../composables/ButtonPrimary";

interface TrendingCoinListProps {
  list: CoinListItemInterface[];
}

interface ColumnsInterface {
  title: string | null;
  dataIndex: keyof CoinListItemInterface;
  type?: string;
  isSortable: boolean;
  width?: number;
}

export default function TrendingPairsSection({ list }: TrendingCoinListProps) {
  const columns: ColumnsInterface[] = [
    {
      title: "Нэр",
      dataIndex: "name",
      type: "icon",
      isSortable: true,
    },
    {
      title: "Үнэ",
      dataIndex: "price",
      type: "number",
      isSortable: true,
    },
    {
      title: "Зах зээлийн нийт үнэлгээ",
      dataIndex: "totalMarketAssessment",
      type: "decimal",
      isSortable: true,
    },
    {
      title: "Эрэлтэд байгаа хэмжээ",
      dataIndex: "demandingAmount",
      type: "number",
      isSortable: true,
    },
    {
      title: "Өөрчлөлт (24 цаг)",
      dataIndex: "change",
      type: "percentage",
      isSortable: false,
    },
    {
      title: null,
      dataIndex: "isAscending",
      type: "chart",
      isSortable: false,
      width: 160,
    },
  ];

  const getCoinIcon = ({ type }: CoinListItemInterface): string => {
    let icon = "";
    if (type == 1) {
      icon = "btc";
    }
    if (type == 2) {
      icon = "eth";
    }
    if (type == 3) {
      icon = "usdt";
    }
    return `static/icons/${icon}-icon.svg`;
  };

  return (
    <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min tw-mt-[52px]">
      <section className="tw-flex tw-justify-between tw-items-end">
        <div className="tw-flex tw-flex-col tw-gap-[6px]">
          <span className="tw-text-[36px] tw-leading-[46.8px] tw-font-[600]">
            Эрэлттэй арилжаа
          </span>
          <span className="tw-text-[24px] tw-leading-[31.2px]">
            Зах зээлийн хэмжээ болон эрэлтийн хэмжээгээр тодорхойлогдсон койн
          </span>
        </div>
        <div>
          <span className="tw-text-secondary tw-text-[24px] tw-leading-[31.2px] tw-cursor-pointer">
            {"Бүх арилжааг харах >"}
          </span>
        </div>
      </section>
      <section className="tw-relative tw-bg-grayLight/20 tw-rounded-[10px] tw-px-[47px] tw-py-[61px] tw-mt-[35px]">
        <div className="tw-absolute tw--z-10 tw-top-1/2 tw-translate-y-[-50%] tw-left-1/2 tw-translate-x-[-50%]">
          <Image
            src={CapexTextVector}
            width={0}
            height={0}
            quality={100}
            alt="capex-text"
            style={{ maxWidth: "1000px", height: "auto" }}
          />
        </div>
        <table className="tw-w-full tw-border-collapse">
          <tbody>
            <tr>
              {columns.map((column) => {
                return (
                  <td
                    key={column.title}
                    className="tw-pb-4"
                    style={{
                      maxWidth: column.width ? `${column.width}px` : "auto",
                    }}
                  >
                    <div
                      className={`tw-inline-flex tw-justify-start tw-items-center tw-gap-2 ${
                        column.isSortable ? "tw-cursor-pointer" : ""
                      }`}
                    >
                      <span className="tw-font-medium tw-text-base tw-leading-[20.8px] tw-text-secondary">
                        {column.title}
                      </span>
                      {column.isSortable && (
                        <Image
                          src={SortIcon}
                          width={10}
                          height={8}
                          quality={100}
                          alt="sort-icon"
                        />
                      )}
                    </div>
                  </td>
                );
              })}
            </tr>
            {list.map((item) => {
              return (
                <tr key={item.id}>
                  {columns.map((column) => {
                    return (
                      <td
                        key={column.title}
                        className={`tw-py-4 ${
                          column.width
                            ? "tw-pl-[43px]"
                            : "tw-border-b tw-border-grayMedium"
                        }`}
                        style={{
                          maxWidth: column.width ? `${column.width}px` : "auto",
                        }}
                      >
                        {column.type == "icon" && (
                          <div className="tw-flex tw-justify-start tw-items-center tw-gap-[5px]">
                            <Image
                              src={getCoinIcon(item)}
                              width={32}
                              height={32}
                              quality={100}
                              alt="coin-icon"
                            />
                            <span>{item[column.dataIndex]}</span>
                          </div>
                        )}
                        {column.type == "number" &&
                          `$${thousandSeparator(item[column.dataIndex])}`}
                        {column.type == "decimal" &&
                          `$${thousandSeparator(item[column.dataIndex])}.00`}
                        {column.type == "percentage" && (
                          <span
                            style={{
                              color: item.isAscending ? "#2EBD85" : "#F5465C",
                            }}
                          >
                            {`${item.isAscending ? "+" : "-"}${
                              item[column.dataIndex]
                            }%`}
                          </span>
                        )}
                        {column.type == "chart" && (
                          <Image
                            src={`static/images/${
                              item.isAscending ? "ascending" : "descending"
                            }-chart.svg`}
                            width={0}
                            height={0}
                            alt="chart-vector"
                            style={{ width: "auto", height: "auto" }}
                          />
                        )}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
      <section className="tw-flex tw-justify-center tw-mt-[76px]">
        <ButtonPrimary
          text={"Бүртгүүлэх"}
          width={266}
          height={59}
          textSize={20}
          textHeight={26}
        />
      </section>
    </div>
  );
}
