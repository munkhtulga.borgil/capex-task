import ButtonPrimary from "../composables/ButtonPrimary";

export default function TradeNowSection() {
  return (
    <div className="tw-flex tw-justify-center tw-mt-[171px]">
      <section className="tw-flex tw-flex-col tw-items-center tw-gap-[35px]">
        <span className="tw-font-[600] tw-text-[36px] tw-leading-[46.8px]">
          Хүлээх хэрэггүй яг одоо эхэл
        </span>
        <ButtonPrimary
          text={"Арилжаа хийх"}
          width={322}
          height={74}
          radius={10}
          textSize={36}
          textHeight={46.8}
          textWeight={600}
        />
      </section>
    </div>
  );
}
