import Image from "next/image";
import Banner from "@/public/static/images/main-page-banner.png";
import ButtonPrimary from "../composables/ButtonPrimary";

export default function BannerSection() {
  return (
    <div className="tw-relative">
      <Image
        src={Banner}
        width={0}
        height={0}
        sizes="100vw"
        style={{ width: "100%", height: "auto" }}
        quality={100}
        priority
        alt="banner"
      />
      <div className="tw-absolute tw-top-[80px] xxl:tw-top-[189px] tw-left-[139px] tw-bottom-[97px]">
        <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min tw-gap-[62px]">
          <section className="tw-flex tw-flex-col tw-gap-[17px]">
            <span className="tw-font-[600] tw-text-[36px] tw-leading-[46.8px] tw-text-white">
              НИЙЛМЭЛ ӨГӨӨЖТЭЙ <br /> STAKING
            </span>
            <span className="tw-text-xl tw-leading-[26px] tw-text-white">
              Capex бирж нь Монгол улсад Modulus-ийн системийн эх кодыг эзэмшиж,<br />
              биржийн үйл ажиллагаа эрхлэн явуулах онцгой эрхтэй “exclusive”
              гэрээг <br /> байгуулаад байна.
            </span>
          </section>
          <section className="tw-flex tw-flex-col tw-gap-[19px]">
            <span className="gradient-text tw-text-[64px] tw-leading-[83.2px] tw-font-[600]">
              12-20%
            </span>
            <ButtonPrimary
              text={"Stake Now"}
              width={322}
              height={74}
              radius={10}
              textSize={36}
              textHeight={46.8}
              textWeight={600}
            />
          </section>
        </div>
      </div>
    </div>
  );
}
