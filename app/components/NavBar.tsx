import Image from "next/image";
import Logo from "@/public/static/capex-logo.svg";
import DarkModeIcon from "@/public/static/icons/dark-theme-icon.svg";
import ButtonPrimary from "./composables/ButtonPrimary";
import ButtonOutlined from "./composables/ButtonOutlined";
import Link from "next/link";

export default function NavBar() {
  interface NavigationMenuType {
    text: string;
    isTrending: boolean;
    route: string;
  }

  const menuItems: NavigationMenuType[] = [
    {
      text: "Зах Зээл",
      isTrending: false,
      route: "/",
    },
    {
      text: "Арилжаа",
      isTrending: false,
      route: "/",
    },
    {
      text: "Онцлох",
      isTrending: false,
      route: "/",
    },
    {
      text: "Байгууллагууд",
      isTrending: false,
      route: "/",
    },
    {
      text: "Урамшуулалт аян",
      isTrending: true,
      route: "/",
    },
    {
      text: "Launchpad",
      isTrending: false,
      route: "/launchpad",
    },
    {
      text: "Мэдээ",
      isTrending: false,
      route: "/",
    },
  ];
  return (
    <div className="tw-px-4 xxl:tw-px-[41px] tw-h-[82px] tw-flex tw-justify-between tw-items-center tw-bg-white">
      <section className="tw-flex tw-justify-start tw-items-center tw-gap-8 xxl:tw-gap-[76px]">
        <Link href={"/"}>
          <Image
            src={Logo}
            width={156}
            quality={100}
            priority
            alt="logo-image"
          />
        </Link>

        <div className="tw-flex tw-justify-start tw-items-center tw-gap-6 xxl:tw-gap-[44px]">
          {menuItems.map((item) => {
            return (
              <Link key={item.text} href={item.route}>
                <button
                  className="tw-relative tw-text-[17px] tw-leading-[22.1px]"
                >
                  {item.text}
                  {item.isTrending && (
                    <span className="tw-absolute tw--top-2 tw--right-2 tw-w-[8px] tw-aspect-square tw-bg-brand"></span>
                  )}
                </button>
              </Link>
            );
          })}
        </div>
      </section>
      <section className="tw-flex tw-justify-start tw-items-center tw-gap-6 xxl:tw-gap-[56px]">
        <div className="tw-flex tw-justify-start tw-gap-[18px]">
          <ButtonOutlined text="Нэвтрэх" />
          <ButtonPrimary text="Бүртгүүлэх" />
        </div>
        <div className="tw-flex tw-justify-start tw-items-center tw-gap-[17px]">
          <span className="tw-text-[17px] tw-cursor-pointer">Eng</span>
          <span className="tw-bg-primary tw-h-5 tw-w-[1px]"></span>
          <span className="tw-text-[17px] tw-cursor-pointer">USD</span>
        </div>
        <Image
          src={DarkModeIcon}
          width={20}
          quality={100}
          priority
          alt="dark-mode"
          style={{ cursor: "pointer" }}
        />
      </section>
    </div>
  );
}
