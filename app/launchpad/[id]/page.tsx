import BannerSection from "@/app/components/launchpad-page/detail-page/BannerSection";
import DescriptionSection from "@/app/components/launchpad-page/detail-page/DescriptionSection";
import PurchaseSection from "@/app/components/launchpad-page/detail-page/PurchaseSection";

interface LaunchPadDetailProps {
  params: {
    id: string | number;
  };
}

export default function LaunchPadDetail({ params }: LaunchPadDetailProps) {
  return (
    <div className="tw-grid tw-grid-cols-1 tw-auto-rows-min">
      <BannerSection />
      <div className="tw-px-[110px] xxl:tw-px-[237px] tw-pt-[46px] tw-pb-[122px] tw-grid tw-grid-cols-2 tw-auto-rows-min tw-gap-[34px]">
        <DescriptionSection />
        <PurchaseSection />
      </div>
    </div>
  );
}
