import BannerSection from "../components/launchpad-page/BannerSection"
import PageReminder from "../components/launchpad-page/PageReminder"
import StatisticsSection from "../components/launchpad-page/StatisticsSection"
import LaunchPadSection from "../components/launchpad-page/LaunchPadSection"

export default function LaunchPad() {
    return (
        <main className="tw-grid tw-grid-cols-1 tw-auto-rows-min">
            <PageReminder />
            <BannerSection />
            <StatisticsSection />
            <LaunchPadSection />
        </main>
    )
}